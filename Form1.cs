﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Data
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        DataTable dt = new DataTable();
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            DialogResult dr = f.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtPath.Text = f.FileName;
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            List<string> ds = new List<string>();

            ds = File.ReadAllLines(txtPath.Text).ToList();
            var dsGoc = ds.Distinct();

            for (int i = 1; i <= 1000; i++)
            {
                DataRow dr = dt.NewRow();
                dr[0] = dsGoc.ElementAt(i - 1).ToString();
                dr[1] = ds.Count(s => s == dr[0].ToString());
                dt.Rows.Add(dr);
            }
            dataGridView1.DataSource = dt;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            dt.Columns.Add("data");
            dt.Columns.Add("Count");
        }
    }
}
